import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/internal/Observable";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  products$: Observable<any>;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.products$ = this.http.get('http://localhost:8080/api/products');

    this.products$.subscribe(products => {
      console.log(products);
    });
  }
}
